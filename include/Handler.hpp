#ifndef XAL_HANDLER_HPP
#define XAL_HANDLER_HPP

#include <SFML/Graphics.hpp>
#include <Dwmapi.h>

namespace xal {
    class Handler {
    public:
        Handler();

        void run();
    private:

    private:
        sf::RenderWindow m_window;

        sf::Texture m_bannerTexture;
        sf::Sprite  m_bannerSprite;

        sf::Font m_font;
        sf::Text m_clockText;

    };
}

#endif
