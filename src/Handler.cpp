#include <Handler.hpp>

namespace xal {
    Handler::Handler() {
        m_bannerTexture.loadFromFile("resources/banner.png");

        m_window.create({m_bannerTexture.getSize().x,m_bannerTexture.getSize().y}, "", sf::Style::None);

        auto desktopVideoMode = sf::VideoMode::getDesktopMode();
        m_window.setPosition(sf::Vector2i(desktopVideoMode.width-m_window.getSize().x-10u, desktopVideoMode.height-m_window.getSize().y-50u));

        // Transparent background
        DWM_BLURBEHIND bb = {0};
        bb.dwFlags = DWM_BB_ENABLE;
        bb.fEnable = true;
        bb.hRgnBlur = NULL;
        DwmEnableBlurBehindWindow(m_window.getSystemHandle(), &bb);
        //-----------------------

        m_bannerSprite.setTexture(m_bannerTexture);

        m_font.loadFromFile("resources/digital-7.ttf");
        m_clockText.setFont(m_font);
        m_clockText.setCharacterSize(60);
        m_clockText.setString("23:10");
        m_clockText.setOrigin(m_clockText.getLocalBounds().left, m_clockText.getLocalBounds().top + m_clockText.getLocalBounds().height/2);
        m_clockText.setPosition(37, m_bannerTexture.getSize().y/2);
    }

    void Handler::run() {
        bool mousePressed = false;
        sf::Vector2i lastMousePos;
        sf::Vector2i mousePos;

        while (m_window.isOpen())
        {
            sf::Event event;
            while (m_window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                    m_window.close();
                else if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left) {
                    mousePressed = true;
                    lastMousePos = sf::Mouse::getPosition();
                } else if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left) {
                    mousePressed = false;
                } else if (event.type == sf::Event::MouseEntered) {
                    //m_window.setSize({m_window.getSize().x, m_window.getSize().y + 50u});
                }
            }

            if (mousePressed) {
                mousePos = sf::Mouse::getPosition();
                if (mousePos != lastMousePos) {
                    m_window.setPosition(m_window.getPosition() + (mousePos - lastMousePos));
                    lastMousePos = mousePos;
                }
            }

            m_window.clear(sf::Color(0,0,0,0));
            m_window.draw(m_bannerSprite);
            m_window.draw(m_clockText);
            m_window.display();
        }
    }
}
