#include <Handler.hpp>

int main() {
    xal::Handler handler;
    handler.run();
    return 0;
}
